package sample;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.event.*;

public class PTController {
    @FXML private TextField soa, sob, soc, ketqua;
    @FXML private Button btnexit;

    private double a, b, c, delta, x1, x2;

    @FXML protected void giai(){
//        System.out.print(Integer.parseInt(soa.getText())/* + Integer.parseInt(sob.getText()) + Integer.parseInt(soc.getText())*/);
        try {
            a = Double.parseDouble(soa.getText().replaceAll(",","."));
            b = Double.parseDouble(sob.getText().replaceAll(",","."));
            c = Double.parseDouble(soc.getText().replaceAll(",","."));

            delta = b*b-4*a*c;

            if(a != 0){
                if(delta > 0){
                    x1 = (-b+Math.sqrt(delta))/(2*a);
                    x2 = (-b-Math.sqrt(delta))/(2*a);
                    ketqua.setText("x1="+Double.toString(x1) + ", x2=" + Double.toString(x2));
                }
                if(delta == 0){
                    x1 = (-b)/(2*a);
                    ketqua.setText("x="+Double.toString(x1));
                }
                if(delta < 0) {
                    ketqua.setText("Phương trình vô nghiệm");
                }
            }
            if(a==0){
                if(b==0){
                    if (c == 0) ketqua.setText("Phương trình có vô số nghiệm");
                    else ketqua.setText("Phương trình vô nghiệm");
                }
                else {
                    x1 = (-c)/b;
                    if(x1 == 0) x1 = 0;
                    ketqua.setText("x="+Double.toString(x1));
                }
            }
        }
        catch (Exception e){
            ketqua.setText("Dữ liệu nhập vào phải là số và không để trống");
        }
    }

    @FXML protected void exit(){
        Stage stage = (Stage) btnexit.getScene().getWindow();
        stage.close();
    }

    @FXML protected void xoarong(){
        soa.setText("");
        sob.setText("");
        soc.setText("");
        ketqua.setText("");
    }

    @FXML protected void about(){
        Dialog dialog = new Dialog();
        dialog.setTitle("Thông tin");
        dialog.setHeaderText("CHƯƠNG TRÌNH GIẢI PHƯƠNG TRÌNH BẬC HAI");
        TextArea textArea = new TextArea();
        textArea.setText("Chương trình giải phương trình bậc hai\n" +
                "Nguyễn Quang Huy\n" +
                "2018");
        textArea.editableProperty().setValue(false);
        dialog.getDialogPane().setContent(textArea);
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.OK);
        dialog.getDialogPane().setPadding(new Insets(10));
        Platform.runLater(()->textArea.requestFocus());
        dialog.getDialogPane().setMaxWidth(350);
        dialog.showAndWait();
    }
}
